from flask import Flask
app = Flask(__name__)

@app.route('/', methods=['GET'])
def firstfunc():
    return "THIS IS ROOT!"

@app.route('/hello/', methods=['GET', 'POST'])
def secondfunc():
    return "Hello World!"
if __name__ == '__main__':
    app.run(host='0.0.0.0')